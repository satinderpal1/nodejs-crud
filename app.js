const express = require("express");
const app = express();
const blogRouter = require("./routes/BlogRoutes");  
const productRouter = require("./routes/ProductRoutes");
require('dotenv').config();
const mongoose = require("mongoose");
//configure mongoose
console.log(process.env.MONGODB_URI);
mongoose.connect(
  process.env.MONGODB_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Connected to MongoDB");
    }
  }
);
 
//middleware
app.use(express.json());

//routes
app.use("/api/blogs", blogRouter);
app.use("/api/products", productRouter)
 
app.listen(3001, () => {
  console.log("Server is running on port 3001");
});
 
module.exports = app;