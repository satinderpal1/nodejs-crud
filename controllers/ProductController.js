const ProductService = require("../services/ProductService");

exports.createProduct = async (req, res) => {
    try{
        const products = await ProductService.createProduct(req.body);
        res.json({data: products, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}
 
exports.getAllProducts = async (req, res) => {
    try {
        const products = await ProductService.getAllProducts();
        res.json({data: products, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.getProductById = async (req, res) => {
    try {
        const product = await ProductService.getProductById(req.params.id);
        res.json({data: product, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.updateProduct = async (req, res) => {
    try {
        const product = await ProductService.updateProduct(req.params.id, req.body);
        res.json({data: product, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.deleteProduct = async (req, res) => {
    try {
        const product = await ProductService.deleteProduct(req.params.id);
        res.json({data: product, status: "success"});
    } catch (err) {
        res.status(500).json({error: err.message});
    }
}